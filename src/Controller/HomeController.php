<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/welcome", name="welcome")
     */
    public function welcome(): Response
    {   
        $name = 'Zoé';
        $module = 'Symfony';
        $classe = 'M1TL';
        $annee = '2021-2022';

        return $this->render('home/welcome.html.twig', [
            'first_name' => $name,
            'module' => $module,
            'classe' => $classe,
            'annee' => $annee
        ]);
    }
}

?>
